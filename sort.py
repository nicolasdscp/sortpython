def bubbleSort(inArr):
    """Sort the given array with bubble sort method
    :param inArr Array given by user
    :return array Sorted array
    """
    arrLen = len(inArr)
    res = inArr
    rngStep = range(arrLen)
    for stepIdx in rngStep:
        rngSort = range(0, arrLen - stepIdx - 1)
        for sortIdx in rngSort:
            if res[sortIdx] > res[sortIdx + 1]:
                res[sortIdx], res[sortIdx + 1] = res[sortIdx + 1], res[sortIdx]
    return res


def mergeSort(inArr):
    """Sort the given array with fusion sort method
    :param inArr Array given by user
    :return array Sorted array
    """
    size = len(inArr)
    if size < 2:
        return inArr
    else:
        middle = size // 2
    return recursiveMergeSort(mergeSort(inArr[:middle]), mergeSort(inArr[middle:]))


def recursiveMergeSort(fArr, sArr):
    """Recursive function of mergeSort()
    :param fArr first part of array
    :param sArr second part of array
    """
    cpt1, cpt2, sizeArr1, sizeArr2 = 0, 0, len(fArr), len(sArr)
    resArr = []
    while cpt1 < sizeArr1 and cpt2 < sizeArr2:
        if fArr[cpt1] < sArr[cpt2]:
            resArr.append(fArr[cpt1])
            cpt1 += 1
        else:
            resArr.append(sArr[cpt2])
            cpt2 += 1
    if cpt1 == sizeArr1:
        resArr.extend(sArr[cpt2:])
    else:
        resArr.extend(fArr[cpt1:])
    return resArr


def quickSort(inArr):
    """Sort the given array with quickSort sort method
    :param inArr Array given by user
    :return array Sorted array
    """
    if inArr == []:
        return []
    else:
        pivot = inArr[0]
        fArr = []
        sArr = []
        for cpt in inArr[1:]:
            if cpt < pivot:
                fArr.append(cpt)
            else:
                sArr.append(cpt)
        return quickSort(fArr) + [pivot] + quickSort(sArr)