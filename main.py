import random
import time

# Import created function
from sort import bubbleSort
from sort import mergeSort
from sort import quickSort

wantedSort = "2"                        # Define wanted sort method

testValues = [                          # Define all test values
    10,
    100,
    1000,
    10000,
    100000
]

sortFunctions = {                       # Map of all sort methods
    "1": {
        "name": "bubble",
        "fnc": bubbleSort,
    },
    "2": {
        "name": "merge",
        "fnc": mergeSort,
    },
    "3": {
        "name": "quick",
        "fnc": quickSort,
    }
}


def main():
    rng = range(len(testValues))
    sort = sortFunctions[wantedSort]

    for cpt in rng:
        file = prepareFile(sort["name"], testValues[cpt])
        randArr = generateArray(testValues[cpt])
        oneTimeTest(file, sort, randArr)
        averageTest(file, sort, randArr)
        file.close()

def prepareFile(methodName, testVals):
    """Open and prepare file for writing
    :param methodName Sort method name
    :return file Prepared file
    """
    file = open("out/" + methodName + "_" + str(testVals) + ".txt", "a")
    file.seek(0)
    file.truncate()
    file.write("Sort method : " + methodName + "\n")

    return file


def oneTimeTest(file, sort, randArr):
    """Do test one time and write result given in file
    :param file File to write result
    :param sort Sort method object
    :param randArr Array of random value to sort
    """
    oneTimeStart = time.time_ns()
    sort["fnc"](randArr)
    oneTimeDiff = time.time_ns() - oneTimeStart
    file.write("One time testing : " + str(oneTimeDiff) + "ns")
    file.write(" / " + str(oneTimeDiff / 1000000000) + "s\n")


def averageTest(file, sort, randArr):
    """Do test 100 times and write result in given file
    :param file File to write result
    :param sort Sort method object
    :param randArr Array of random value to sort
    """
    rng = range(100)
    timeValues = []
    for cpt in rng:
        print("Start test #" + str(cpt) + "...")
        oneTimeStart = time.time_ns()
        sort["fnc"](randArr)
        timeValues.append(time.time_ns() - oneTimeStart)
    averageNS = sum(timeValues) / 100
    file.write("Average : " + str(averageNS) + "ns")
    file.write(" / " + str(averageNS / 1000000000) + "s\n")
    for cpt in rng:
        file.write("\ttest #" + str(cpt) + ": " + str(timeValues[cpt]) + "ns\n")


def generateArray(size):
    """Generate list of random int from 0 to 2e32 - 1
    :param size Size of array
    :return array Composed of random unsigned int
    """
    randArr = []
    rng = range(size)
    for cpt in rng:
        randArr.append(random.randint(0, pow(2, 32) - 1))
    return randArr

main()
